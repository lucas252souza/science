import numpy as np
from scipy.optimize import linprog
from generator_conditional import generator_conditional
from benders_linear import benders_decomposition_linear_original

def to_list(vet):
    """
        Transforma um vetor de vetores unitarios em uma lista unica.
    """
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

def cria_tabela(r, c, rho, pi, rho_r, pi_r):
    """
        Cria uma tabela com base na tabela que o simplex cria para ser executado.
    """
    ident = np.eye(r)
    zeros = np.zeros(r)

    # Concatena as funcao objetivo
    # Concatena as restricoes
    # Adiciona as variaveis de folga
    # Adiciona zeros a funcao objetivo
    # Multiplica funcao objetivo por -1 para ficar todos negativos
    # Adiciona um zero ao c
    # Modifica o shape do c para (r+1, 1)
    funcao_objetivo = np.concatenate((rho_r, pi_r))
    restricao = np.concatenate((rho.T, pi.T), axis=1)
    funcao_objetivo = to_list(funcao_objetivo)
    restricao_expandida = np.concatenate((restricao, ident), axis = 1)
    funcao_objetivo = np.concatenate((funcao_objetivo, zeros))
    funcao_objetivo = np.dot(-1, funcao_objetivo)
    c = np.concatenate(([0], c))
    c = np.array([c]).T

    # Concatenacao Geral
    horizontal = np.concatenate(([funcao_objetivo], restricao_expandida))
    vertical = np.concatenate((horizontal, c), axis=1)

    # Retorna uma tabela
    return vertical

def filtro(tabela):
    """
        Aplica um filtro em cima da tabela semelhante a do simplex.
        O filtro baseia na divisao da ultima coluna pela coluna com menor membro da primeira linha.
        Comparado com a divisao da ultima linha da ultima coluna pela ultima linha da coluna selecionada.
    """
    # Pega o minimo da primeira linha
    menor = np.min(tabela[0])
    # Indice do minimo da primeira linha
    indice = np.where(tabela[0] == menor)[0][0]
    # Pega a coluna pertencente ao minimo
    colum = tabela[:, indice:indice+1]
    # Pega a ultima coluna da tabela
    ult_colum = tabela[:, -1:]

    # Iteracao para realizar as divisoes da ultima coluna pela coluna de indice do menor
    divisao = []
    n = colum.size
    i = 0
    while i < n:
        divisao.append(ult_colum[i]/colum[i])
        i+=1

    # Minimo valor das divisoes
    minimo = np.max(divisao)

    # Divisao do valor i da ultima coluna pelo i da coluna do indice menor
    valor_de_verificacao = ult_colum[-1]/colum[-1]

    if minimo == valor_de_verificacao:
        return True
    else:
        return False


def aplica_filtro(r):
    """
        Aplica o filtro, caso de verdadeiro, gera novos dados, caso falto, retorna os dados.
    """
    c, rho, pi, rho_r, pi_r, Q = generator_conditional(r, r-1, r-1)
    tabela = cria_tabela(r, c, rho, pi, rho_r, pi_r)
    verifica = filtro(tabela)

    if verifica:
        (c, rho, pi, rho_r, pi_r, Q) = aplica_filtro(r)

    return (c, rho, pi, rho_r, pi_r, Q)

def prepara_dados(c, rho, pi, rho_r, pi_r):
    """
        Prepara os dados para aplicacao do Simplex no formato do problema proposto.
    """
    z = np.concatenate((rho_r, pi_r))
    z = to_list(z)
    z = np.dot(-1, z)
    st = np.concatenate((rho.T, pi.T), axis=1)

    return (z, st, c)

c, rho, pi, rho_r, pi_r, Q = aplica_filtro(5)
z, Aub, bub = prepara_dados(c, rho, pi, rho_r, pi_r)

print("-------------------------------Simplex------------------------------------------")
res = linprog(c=z, A_ub=Aub, b_ub=bub)
print ("Valor Optmo: ", res.fun)
print ("Num iteracao: ", res.nit)
if res.nit == 1:
    print("-------Funcao Objetivo-----------")
    print(z)
    print("------Restricao-------------")
    print(Aub)
    print("-------Desigualdade---------")
    print(bub)
print("------------------------------Fim Simplex---------------------------------------")

print("------------------------------Benders-------------------------------------------")
(x0, x, y, it, n, num) = benders_decomposition_linear_original(rho_r.T, pi_r.T, rho.T, pi.T, c, Q)
print ("Valor Optmo: ", x0)
print ("Num iteracao: ", n)
print("-----------------------------Fim Benders----------------------------------------")
