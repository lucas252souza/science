# -*- coding: utf-8 -*-

from sets import Set
from scipy.optimize import linprog
import numpy as np
from generator_conditional import generator_conditional
import sys
import pandas as pd
import datetime

# ------------------------------------------------------------------------------------------------------------------
# Testes com gerador
'''
r = 1000
l = 999
e = 999
c, rho, pi, rho_r, pi_r, Q = generator_conditional(r, l, e)

def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

"""
c_simplex = np.concatenate((rho_r.tolist(), pi_r.tolist()))
c_simplex = to_list(c_simplex)
A_ub = np.concatenate((rho, pi))
b_ub = c
bound = (None, None)
"""

res = linprog(c=c, A_ub=rho, b_ub=rho_r, A_eq=pi, b_eq=pi_r, method='interior-point',
                options={"sym_pos": False})
print "Results from simplex"
print "x0: ", res.fun
print "n: ", res.nit

'''
# ------------------------------------------------------------------------------------------------------------------
# Testes com loop


# Função para transformar uma vetor de vetores unitarios em uma lista normal
def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

# --------------------------------------- Dados --------------------------------
cont = 100
i = 1
r = 50
e = 49
l = 49

resses = [] # Valor Otimo
iteracao_pi = [] # Numero de iteraçoes
ordens = [] # Valor da ordem de 'r'
times_pi = [] # Tempo do simplex


# ----------------------------------- Loop Do Teste ----------------------------
while i <= cont:
    print "Run :", i
    print "Ordem : ", r

    # Gera dados
    c, rho, pi, rho_r, pi_r, Q = generator_conditional(r, e, l)

    # Prepara os Dados
    c_concat = np.concatenate((rho_r.tolist(), pi_r.tolist()))
    c_concat = to_list(c_concat)
    A_ub = np.concatenate((rho, pi))
    b_ub = c
    
    # Executa o Linprog com Ponto Interior
    pi_time_before = datetime.datetime.now()
    res = linprog(c=np.dot(-1, c_concat), A_ub=A_ub.T, b_ub=c, method='interior-point',
                    options={"sym_pos": False})
    pi_time_after = datetime.datetime.now()

    # Salva os dados do PI
    ordens.append(r)
    resses.append(res.fun)
    iteracao_pi.append(res.nit)

    time = (pi_time_after - pi_time_before)
    sec = time.seconds
    micro = time.microseconds

    tempo = str(sec) + "." + str(micro)
    times_pi.append(float(tempo))

    if(i % 5 == 0):
        r += 50
        e += 50
        l += 50

    i += 1

# -------------------------------- Salvar Dados --------------------------------

# Cria Data Frame para guardar os valores e exportar Excel
df = pd.DataFrame()

df["Ordem"] = ordens
df["Valor Optimo"] = resses
df["Numero de Iteracao"] = iteracao_pi
df["Tempo (sec:μsec)"] = times_pi

# Exporta Excel
df.to_excel("Problema_Dual.xlsx")
