# -*- coding: utf-8 -*-

from sets import Set
from scipy.optimize import linprog
import numpy as np
from generator_conditional import generator_conditional
from benders_linear import benders_decomposition_linear_original
from benders_linear import benders_decomposition_linear_original_bounded
from benders_linear import benders_decomposition_linear_alternative
import sys
import pandas as pd
import datetime

# Função para transformar uma vetor de vetores unitarios em uma lista normal
def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

# Função para preparar os dados para aplicar o Dual
def prepara_dual(c, rho, pi, rho_r, pi_r):
    funcao_objetivo = np.concatenate((rho_r, pi_r))
    restricao = np.concatenate((rho.T, pi.T), axis=1)
    funcao_objetivo = to_list(funcao_objetivo)
    funcao_objetivo = np.dot(-1, funcao_objetivo)

    return(funcao_objetivo, restricao, c)

# --------------------------------------- Dados --------------------------------

r = 5
e = 4
l = 4
M = 1000000

np.random.seed(20)
c, rho, pi, rho_r, pi_r, Q = generator_conditional(r, e, l)
'''
print("c:", c)
print("rho: ", rho)
print("pi: ", pi)
print("Q: ", Q)
'''
    # Prepara os dados
objetivo, restricao, vetor = prepara_dual(c, rho, pi, rho_r, pi_r)
bound = [(None, None),]* rho_r.size + [(0, None), ] * pi_r.size

    # Executa o Benders com Simplex
(x0, x, y, it, n) = benders_decomposition_linear_original(rho_r.T, pi_r.T, rho.T, pi.T, c, Q)

    # Executa o Linprog com Simplex
simplex = linprog(c=objetivo, A_ub=restricao, b_ub=vetor, bounds=bound)

print("--- Benders ---")
print("Optimo: ", x0)
print("Iteracao: ", n)

print("--- Simplex ---")
print("Optimo: ", simplex.fun)
print("Iteracao: ", simplex.nit)
