# -*- coding: utf-8 -*-

from sets import Set
from scipy.optimize import linprog
import numpy as np
from generator_conditional import generator_conditional
from benders_linear import benders_decomposition_linear_original
import sys
import pandas as pd
import datetime

'''
# ------------------------------------------------------------------------------------------------------------------
# Testes com gerador

r = 1000
l = 999
e = 999
c, rho, pi, rho_r, pi_r, Q = generator_conditional(r, l, e)

def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

c_concat = np.concatenate((rho_r.tolist(), pi_r.tolist()))
c_concat = to_list(c_concat)
A_ub = np.concatenate((rho, pi))
b_ub = c

res = linprog(c=np.dot(-1, c_concat), A_ub=A_ub.T, b_ub=c, method='interior-point')
print "Results from linprog"
print "Valor: ", res.fun
print "nit: ", res.nit

'''
# ------------------------------------------------------------------------------------------------------------------
# Testes com loop


# Função para transformar uma vetor de vetores unitarios em uma lista normal
def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

# --------------------------------------- Dados --------------------------------
cont = 60
i = 1
r = 5
e = 4
l = 4

iteracao_pi = [] # Numero de iteracoes
iteracao_simplex = [] # Numero de iteracoes
iteracao_benders = [] # Numero de iteracoes
ordens = [] # Valor da ordem de 'r'



# ----------------------------------- Loop Do Teste ----------------------------
while i <= cont:
    print "Run :", i
    print "Ordem : ", r

    # Gera dados
    c, rho, pi, rho_r, pi_r, Q = generator_conditional(r, e, l)

    # Prepara os dados
    c_concat = np.concatenate((rho_r.tolist(), pi_r.tolist()))
    c_concat = to_list(c_concat)
    A_ub = np.concatenate((rho, pi))
    b_ub = c

    # Executa o Benders com Simplex
    (x0, x, y, it, n, num) = benders_decomposition_linear_original(rho_r.T, pi_r.T, rho.T, pi.T, c, Q)

    # Se Benders for inf, vai para o proximo loop
    if np.isinf(x0):
        continue

    # Executa o Linprog com Ponto Interior
    pi = linprog(c=np.dot(-1, c_concat), A_ub=A_ub.T, b_ub=c, method='interior-point',
                options={"sym_pos": False})

    # Executa o Linprog com Simplex
    simplex = linprog(c=np.dot(-1, c_concat), A_ub=A_ub.T, b_ub=c)

    # Salva os dados
    ordens.append(r)
    iteracao_benders.append(n)
    iteracao_simplex.append(simplex.nit)
    iteracao_pi.append(pi.nit)

    if(i % 10 == 0):
        r += 5
        e += 5
        l += 5

    i += 1

# -------------------------------- Salvar Dados --------------------------------

# Cria Data Frame para guardar os valores e exportar Excel
df = pd.DataFrame()

df["Ordem"] = ordens
df["Iteracao Benders"] = iteracao_benders
df["Iteracao Simplex"] = iteracao_simplex
df["Iteracao PI"] = iteracao_pi

# Exporta Excel
df.to_excel("PIxSimplex-Benders.xlsx")
