# -*- coding: utf-8 -*-

from sets import Set
from scipy.optimize import linprog
import numpy as np
from generator_conditional import generator_conditional
from benders_linear import benders_decomposition_linear_original
import sys
import pandas as pd
import datetime

# Função para transformar uma vetor de vetores unitarios em uma lista normal
def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

# Função para preparar os dados para aplicar o Dual
def prepara_dual(c, rho, pi, rho_r, pi_r):
    funcao_objetivo = np.concatenate((rho_r, pi_r))
    restricao = np.concatenate((rho.T, pi.T), axis=1)
    funcao_objetivo = to_list(funcao_objetivo)
    funcao_objetivo = np.dot(-1, funcao_objetivo)

    return(funcao_objetivo, restricao, c)
# --------------------------------------- Dados --------------------------------
cont = 30
i = 1
r = 5
e = 4
l = 4

otimo_benders = []
otimo_simplex = []
iteracao_simplex = [] # Numero de iteracoes
iteracao_benders = [] # Numero de iteracoes
ordens = [] # Valor da ordem de 'r'



# ----------------------------------- Loop Do Teste ----------------------------
while i <= cont:
    print "Run :", i
    print "Ordem : ", r

    # Gera dados
    c, rho, pi, rho_r, pi_r, Q = generator_conditional(r, e, l)

    # Prepara os dados
    objetivo, restricao, vetor = prepara_dual(c, rho, pi, rho_r, pi_r)
    bound = [(None, None),]* rho_r.size + [(0, None), ] * pi_r.size
    # Executa o Benders com Simplex
    (x0, x, y, it, n, num) = benders_decomposition_linear_original(rho_r.T, pi_r.T, rho.T, pi.T, c, Q)

    # Executa o Linprog com Simplex
    simplex = linprog(c=objetivo, A_ub=restricao, b_ub=vetor, bounds=bound)

    # Se Benders for inf, vai para o proximo loop
    if np.isinf(x0) or x0 != (-1.0)*simplex.fun:
        continue

    # Salva os dados
    ordens.append(r)
    iteracao_benders.append(n)
    iteracao_simplex.append(simplex.nit)
    otimo_benders.append(x0)
    otimo_simplex.append(simplex.fun)


    if(i % 10 == 0):
        r += 5
        e += 5
        l += 5

    i += 1

# -------------------------------- Salvar Dados --------------------------------
# Cria Data Frame para guardar os valores e exportar Excel
df = pd.DataFrame()

df["Ordem"] = ordens
df["Iteracao Benders"] = iteracao_benders
df["Iteracao Simplex"] = iteracao_simplex
df["Otimo Benders"] = otimo_benders
df["Otimo Simplex"] = otimo_simplex

# Exporta Excel
df.to_excel("Simplex-Benders.xlsx")
