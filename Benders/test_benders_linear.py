# -*- coding: utf-8 -*-

from sets import Set
from scipy.optimize import linprog
import numpy as np
from benders_linear import benders_decomposition_linear_original
from benders_linear import benders_decomposition_linear
from generator_conditional import generator_conditional
import sys
import pandas as pd
import datetime

# ------------------------------------------------------------------------------------------------------------------
# Testes com valor fixo

rho = np.array([[1, 3, 5], [2, 4, 6]])
pi = np.array([[7, 9, 11], [8, 10, 12]])
rho_r = np.array([5, 6])
pi_r = np.array([11, 12])
c = np.array([4, 5, 7])
Q = set([tuple([1, 0, 0, 1])])

(x0, x, y, it, n, num) = benders_decomposition_linear_original(rho_r.T, pi_r.T, rho.T, pi.T, c, Q)
print "Results from benders_decomposition_linear"
print "x0 = ", x0
print "x = ", x
print "y = ", y
print "it = ", it
print "n = ", n

c_simplex = np.concatenate((rho_r.tolist(), pi_r.tolist()))
A_ub = np.concatenate((rho, pi))
b_ub = c
res = linprog(A_ub = A_ub.T, b_ub = b_ub, c= -c_simplex)
print "Results from Simplex Only"
print res
'''

# ------------------------------------------------------------------------------------------------------------------
# Testes com gerador

r = 5
l = 4
e = 4
c, rho, pi, rho_r, pi_r, Q = generator_conditional(r, l, e)

M = 2000

# solves using benders decomposition
benders_time_before = datetime.datetime.now()
(x0, x, y, it, n, num) = benders_decomposition_linear_original(rho_r.T, pi_r.T, rho.T, pi.T, c, Q)
benders_time_after = datetime.datetime.now()

#(x0, x, y, it, n, b) = benders_decomposition_linear(rho_r.T, pi_r.T, rho.T, pi.T, c, M, Q)

print "Results from benders_decomposition_linear"
print "x0 = ", x0
print "n = ", n

def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

c_simplex = np.concatenate((rho_r.tolist(), pi_r.tolist()))
c_simplex = to_list(c_simplex)
A_ub = np.concatenate((rho, pi))
b_ub = c
bound = (None, None)
# solves original problem with only one simplex
simplex_time_before = datetime.datetime.now()
res = linprog(A_ub = A_ub.T, b_ub = b_ub, c= -c_simplex, bounds=bound)
simplex_time_after = datetime.datetime.now()
print "Results from simplex alone"
print "x0: ", res.fun
print "n: ", res.nit

print "Benders Time: ", benders_time_after - benders_time_before
print "Simplex time: ", simplex_time_after - simplex_time_before


# ------------------------------------------------------------------------------------------------------------------
# Testes com loop
'''
'''
# Função para transformar uma vetor de vetores unitarios em uma lista normal
def to_list(vet):
    lista = []
    for x in vet:
        lista.append(x[0])

    return np.array(lista)

# --------------------------------------- Dados --------------------------------
cont = 100
i = 0
times_benders = []
times_simplex = []
resses = []
xisses = []
norma_tempo = []
norma_valor = []
iteracao_benders = []
iteracao_simplex = []
numero_de_simplex = []

# ----------------------------------- Loop Do Teste ----------------------------
while i < cont:
    print "Run :", i

    # Gera dados
    c, rho, pi, rho_r, pi_r, Q = generator_conditional(25, 24, 24)

    # Executa Benders e calcula tempo
    benders_time_before = datetime.datetime.now()
    (x0, x, y, it, n, num) = benders_decomposition_linear_original(rho_r.T, pi_r.T, rho.T, pi.T, c, Q)
    benders_time_after = datetime.datetime.now()

    # Se Benders for inf, vai para o proximo loop
    if np.isinf(x0):
        continue

    # Salva os dados do Benders
    xisses.append(x0)
    iteracao_benders.append(n)
    numero_de_simplex.append(num)

    # Prepara os dados para o Simplex
    c_simplex = np.concatenate((rho_r.tolist(), pi_r.tolist()))
    c_simplex = to_list(c_simplex)
    A_ub = np.concatenate((rho, pi))
    b_ub = c

    # Executa o Simplex e calcula tempo
    simplex_time_before = datetime.datetime.now()
    res = linprog(A_ub = A_ub.T, b_ub = b_ub, c= -c_simplex)
    simplex_time_after = datetime.datetime.now()

    # Salva os dados do Simplex
    resses.append(res.fun)
    iteracao_simplex.append(res.nit)

    # Calcula os tempos limites e salva
    times_benders.append((benders_time_after - benders_time_before).microseconds)
    times_simplex.append((simplex_time_after - simplex_time_before).microseconds)

    i += 1

# -------------------------------- Salvar Dados --------------------------------

# Passar valores otimos do Simplex para positivos
resses = np.dot(-1,resses)

# Listas de Modulos da diferença de tempos e diferença de valores
for j in range(100):
    minus = times_simplex[j] - times_benders[j]
    if minus < 0:
        minus *= -1
    norma_tempo.append(minus)

for k in range(100):
    minus = resses[k] - xisses[k]
    if minus < 0:
        minus *= -1
    norma_valor.append(minus)

# Cria Data Frame para guardar os valores e exportar Excel
df = pd.DataFrame()
df["Tempo Benders (μs)"] = times_benders
df["Tempo Simplex (μs)"] = times_simplex
df["Valor Benders"] = xisses
df["Valor Simplex"] = resses
df["Iteração Benders"] = iteracao_benders
df["Iteração Simplex"] = iteracao_simplex
df["Numero de Simplex"] = numero_de_simplex
df["Ts - Tb"] = norma_tempo
df["Vs - Vb"] = norma_valor
df["Media Ts"] = np.mean(times_simplex)
df["Media Tb"] = np.mean(times_benders)
df["Media |Ts - Tb|"] = np.mean(norma_tempo)
df["Media |Vs - Vb|"] = np.mean(norma_valor)

# Exporta Excel
df.to_excel("r25.xlsx")
'''
