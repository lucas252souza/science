import numpy as np

def data_generator(r, l, e, tam = 1000):
    '''
        Entrada:
            r, l, e: Numeros Naturais maiores que zero
            tam : Valor maximo de cada elemento, padrao: 1000.
        Saida:
            vetor: tamanho r, matriz: ordem l x r, matriz: ordem e x r
        Erro:
            tupla: None, print do erro.
    '''
    less_than = l <= (r-1) and e <= (r-1)
    greather_than_zero = l > 0 and e > 0 and r > 0

    if not (less_than and greather_than_zero):
        print("Data Generator Error: Bad Entry.")
        return (None, None, None)
    else:
        vector = np.random.randint(0, tam+1, size = r)
        matriz_lr = np.random.randint(0, tam+1, size = (l, r))
        matriz_er = np.random.randint(0, tam+1, size = (e, r))

        return (vector, matriz_lr, matriz_er)
